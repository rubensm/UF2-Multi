package com.example.rlab.test;

import android.app.Activity;
import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;

public class MainActivity extends Activity implements View.OnClickListener {

    private SoundPool sp;
    private AudioManager am;
    private Button playSound1;
    private Button playSound2;
    private Button stopAllSounds;
    private int sound1;
    private int sound2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //el constructor int maxStreams, int streamType, int srcQuality
        sp = new SoundPool(2, AudioManager.STREAM_MUSIC, 0);
        //utilitzem el audiomanager amb el serveu d'audio
        am = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        //carregem els sons
        sound1 = sp.load(this, R.raw.laser_machine, 1);
        sound2 = sp.load(this, R.raw.r2d2, 1);
        //carregem botons
        playSound1 = (Button) findViewById(R.id.sound1);
        playSound1.setOnClickListener(this);
        playSound2 = (Button) findViewById(R.id.sound2);
        playSound2.setOnClickListener(this);
        stopAllSounds = (Button) findViewById(R.id.stop);
        stopAllSounds.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        float streamVolume = am.getStreamVolume(AudioManager.STREAM_MUSIC);
        streamVolume = streamVolume / am.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        if (v.getId() == R.id.sound1) {
            sp.play(sound1, streamVolume, streamVolume, 1, 1, 1f);
        } else if (v.getId() == R.id.sound2) {
            sp.play(sound2, streamVolume, streamVolume, 1, 1, 1f);
        } else if (v.getId() == R.id.stop) {
            sp.autoPause();
        }
    }

}
