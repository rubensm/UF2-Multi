package com.example.rlab.audiointro;

import android.app.Activity;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends Activity implements View.OnClickListener {
    private MediaPlayer mp;
    Button raw;
    Button uri;
    Button stop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        raw = (Button) findViewById(R.id.braw);
        raw.setOnClickListener(this);
        uri = (Button) findViewById(R.id.buri);
        uri.setOnClickListener(this);
        stop = (Button) findViewById(R.id.stop);
        stop.setOnClickListener(this);
    }

    //creem el metode per executar la canço de raw
    private void playRawSong() {
        mp = MediaPlayer.create(this, R.raw.bj1minute);
        mp.start();
    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.braw) {
            playRawSong();
        } else if (v.getId() == R.id.buri) {
            new tascaAsincrona().execute();
        } else if (v.getId() == R.id.stop) {
            Toast.makeText(MainActivity.this, "Falta fer be el botó aturar", Toast.LENGTH_SHORT).show();
            mp.stop();
        }
    }

    //per fer la tasca feixuda de fer la connexio a internet
    private class tascaAsincrona extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            mp = new MediaPlayer();
            try {
                //agafem informacio d'internet(cuidado con los permisos del manifest)
                Uri uri = Uri
                        .parse("http://upload.wikimedia.org/wikipedia/pt/8/80/Depeche_Mode_-_Enjoy_The_Silence_%281990%29.ogg");
                if (uri != null) {
                    mp = MediaPlayer.create(MainActivity.this, uri);
                    mp.start();
                }
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(MainActivity.this, "Uri incorrecte", Toast.LENGTH_SHORT).show();
            }

            return null;
        }
    }

}

