package com.example.rlab.video;

import android.app.Activity;
import android.media.session.MediaController;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;
import android.widget.VideoView;

public class MainActivity extends Activity implements View.OnTouchListener {
    private boolean firstTouch = true;
    private VideoView vv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //recuperem el videoview
        vv = (VideoView) findViewById(R.id.vidView);
        //creem objecte media controller
        android.widget.MediaController mc = new android.widget.MediaController(MainActivity.this);
        //asignem la video view
        mc.setAnchorView(vv);
        vv.setMediaController(mc);
        //pasem la ruta
        vv.setVideoPath("android.resource://com.example.rlab.video/" + R.raw.argo);
        //posem el listener
        vv.setOnTouchListener(this);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (firstTouch) {
            vv.start();
            Toast.makeText(MainActivity.this, "Primer cop que premo", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(MainActivity.this, "No es el primer cop que premo", Toast.LENGTH_SHORT).show();
        }
        firstTouch = false;
        return false;
    }

}
